package be.odisee.spring;

import be.odisee.spring.domain.Quote;
import be.odisee.spring.persistence.QuoteRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    CommandLineRunner initDB(QuoteRepository repository) {
        return (args) -> {
            repository.save(new Quote(
                    "Edsger Dijkstra",
                    "If debugging is the process of removing software bugs, then programming must be the process of putting them in")
            );
            repository.save(new Quote(
                    "Bill Gates",
                    "Measuring programming progress by lines of code is like measuring aircraft building progress by weight.")
            );
            repository.save(new Quote(
                    "Jessica Gaston",
                    "One man’s crappy software is another man’s full time job.")
            );
            repository.save(new Quote(
                    "Michael Sinz",
                    "Programming is like sex. One mistake and you have to support it for the rest of your life.")
            );
            repository.save(new Quote(
                    "Martin Golding",
                    "Always code as if the guy who ends up maintaining your code will be a violent psychopath who knows where you live.")
            );
        };
    }

}
