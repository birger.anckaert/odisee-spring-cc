package be.odisee.spring.web;

class CreateQuote {
    private String author;
    private String text;

    private CreateQuote(){}

    protected CreateQuote(String author, String text) {
        this.author = author;
        this.text = text;
    }

    public String getAuthor() {
        return author;
    }

    public String getText() {
        return text;
    }

}
