package be.odisee.spring.web;

import be.odisee.spring.domain.Quote;
import be.odisee.spring.service.QuoteService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("quotes")
public class QuoteController {

    private final QuoteService service;

    QuoteController(QuoteService service) {
        this.service = service;
    }

    @GetMapping(
            path = "/random",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Quote getRandomQuote() {
        return service.getRandomQuote();
    }

    @GetMapping(
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public List<Quote> getQuotes() {
        return service.getQuotes();
    }

    @GetMapping(
            path = "/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Quote getQuote(@PathVariable long id) {
        return service.getQuote(id);
    }

    @PostMapping(
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Quote createQuote(@RequestBody CreateQuote createQuote) {
        return service.createQuote(createQuote.getAuthor(), createQuote.getText());
    }

    @PutMapping(
            path = "/{id}",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Quote updateQuote(@PathVariable long id, @RequestBody CreateQuote createQuote) {
        return service.updateQuote(id, createQuote.getAuthor(), createQuote.getText());
    }

    @DeleteMapping(path = "/{id}")
    public void deleteQuote(@PathVariable long id) {
        service.deleteQuote(id);
    }
}
