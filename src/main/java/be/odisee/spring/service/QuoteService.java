package be.odisee.spring.service;

import be.odisee.spring.domain.Quote;
import be.odisee.spring.persistence.QuoteRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.SplittableRandom;

@Service
public class QuoteService {

    private static final SplittableRandom rg = new SplittableRandom();

    private final QuoteRepository repository;

    QuoteService(QuoteRepository repository) {
        this.repository = repository;
    }

    public Quote getRandomQuote() {
        long max = repository.count();
        long id = 1 + rg.nextLong(max);
        return repository.findById(id).orElse(null);
    }

    public List<Quote> getQuotes() {
        List<Quote> quotes = new ArrayList<>();
        repository.findAll().forEach(quotes::add);
        return quotes;
    }

    public Quote getQuote(long id) {
        return repository
                .findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Quote Not Found"));
    }

    public Quote createQuote(String author, String text) {
        return repository.save(new Quote(author, text));
    }

    public Quote updateQuote(long id, String author, String text) {
        Quote quote = getQuote(id);
        quote.setAuthor(author);
        quote.setText(text);
        return repository.save(quote);
    }

    public void deleteQuote(long id) {
        repository.deleteById(id);
    }
}
