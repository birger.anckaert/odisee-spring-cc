package be.odisee.spring.persistence;

import be.odisee.spring.domain.Quote;
import org.springframework.data.repository.CrudRepository;

public interface QuoteRepository extends CrudRepository<Quote, Long> {
}
